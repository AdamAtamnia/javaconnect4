public class Board {
    private Tile[][] gameBoard;
    private final int WIDTH = 7;
    private final int HEIGHT = 6;

    //Fills the gameBoard array with Tile objects.
    public Board(){
        this.gameBoard = new Tile[this.HEIGHT][this.WIDTH];
        for (int i = 0; i < gameBoard.length; i++) {
            for (int j = 0; j < gameBoard[i].length; j++) {
                gameBoard[i][j] = new Tile();
            }
        }
    }
    
    //Prints the board. Remember: Bottom row is the i = 0 array to facilitate placing the tokens.
    public String toString(){
        String build = "\n 1 2 3 4 5 6 7\n---------------\n";
        for (int i = gameBoard.length - 1; i >= 0; i--) {
            build = build + "|";
            for (int j = 0; j < gameBoard[i].length; j++) {
                build = build + gameBoard[i][j] + "|";
            }
            build = build + "\n---------------\n";
        }
        return build;
    }

    //Resets the board making all tiles blank.
    public void reset(){
        for (int i = 0; i < gameBoard.length; i++) {
            for (int j = 0; j < gameBoard[i].length; j++) {
                gameBoard[i][j].setType("blank");
            }
        }
    }

    //Places a token on top of the other tokens in the same column, if any.
    public void placeToken(int pos, String playerColor){
        if (pos < 1 || pos > 7){
            throw new IllegalArgumentException("Place token position needs to be between 1 and 7");
        } else if (gameBoard[gameBoard.length - 1][pos - 1].getType() == "red" 
        || gameBoard[gameBoard.length - 1][pos - 1].getType() == "yellow"){
            throw new IllegalArgumentException("Column full!");
        }
        //Remember: Bottom row is the i = 0 array.
        for (int i = 0; i < gameBoard.length; i++) {
            if (gameBoard[i][pos - 1].getType() == "blank"){
                gameBoard[i][pos - 1].setType(playerColor);
                break; 
            } 
        }
    }

    //Returns a custom error status to facilitate validation of input.
    public String inputValidation(int pos){
        if (pos < 1 || pos > 7){
            return "OutOfBounds";
        } else if (gameBoard[gameBoard.length - 1][pos - 1].getType() == "red" 
        || gameBoard[gameBoard.length - 1][pos - 1].getType() == "yellow"){
            return "ColumnFull";
        } else {
            return "Success";
        }
    }

    //Checks if there is a tie by checking if all the upper rows are full.
    public boolean isTie(){
        boolean isTie = false;
        int countFull = 0;
        for (int i = 0; i < gameBoard[gameBoard.length - 1].length; i++) {
            if (gameBoard[gameBoard.length - 1][i].getType() == "red" 
            || gameBoard[gameBoard.length - 1][i].getType() == "yellow"){
                countFull++;
            }
        }
        if (countFull == gameBoard[gameBoard.length - 1].length){
            isTie = true;
        }
        return isTie;
    }

    //Checks for the winner (connect 4) in all directions. 
    public boolean checkWin(String playerColor){
        boolean isWin = false;
        if(checkWinStraitHorizontal(playerColor) == true 
        || checkWinStraitVertical(playerColor) == true 
        || checkWinDiagonalAscending(playerColor) == true 
        || checkWinDiagonalDescending(playerColor) == true){
            isWin = true;
        }
        return isWin;
    }

    //Checks for horizontal connect fours. 
    public boolean checkWinStraitHorizontal(String playerColor){
        boolean isWin = false;
        int count = 0;
        for (int i = 0; i < gameBoard.length; i++) {
            count = 0;
            for (int j = 0; j < gameBoard[i].length; j++) {
                if(gameBoard[i][j].getType() == playerColor){
                    count++;
                    if (count == 4){
                        isWin = true;
                        break;
                    }
                } else {
                    count = 0;
                }
            }
        }
        return isWin;
    }

    //Checks for vertical connect fours.
    public boolean checkWinStraitVertical(String playerColor){
        boolean isWin = false;
        int count = 0;
        for (int i = 0; i < gameBoard.length - 3; i++) {
            count = 0;
            for (int j = 0; j < gameBoard[i].length; j++) {
                if(gameBoard[i][j].getType() == playerColor){
                    for (int c = i; c < i+4; c++) {
                        if(gameBoard[c][j].getType() == playerColor){
                            count++;
                            if (count == 4){
                                isWin = true;
                                break;
                            }
                        } else {
                            count = 0;
                        }
                    }
                }
            }
        }
        return isWin;
    }

    //Checks for ascending diagonal connect fours.
    public boolean checkWinDiagonalAscending(String playerColor){
        boolean isWin = false;
        int count = 0;
        for (int i = 0; i < gameBoard.length - 3; i++) {
            for (int j = 0; j < gameBoard[i].length - 3; j++) {
                count = 0;
                if(gameBoard[i][j].getType() == playerColor){
                    for (int c = 0; c < 4; c++) {
                        if(gameBoard[i+c][j+c].getType() == playerColor){
                            count++;
                            if (count == 4){
                                isWin = true;
                                break;
                            }
                        } else {
                            count = 0;
                        }
                    }
                }
            }
        }
        return isWin;
    }

    //Checks for descending diagonal connect fours.
    public boolean checkWinDiagonalDescending(String playerColor){
        boolean isWin = false;
        int count = 0;
        for (int i = 0; i < gameBoard.length - 3; i++) {
            for (int j = 3; j < gameBoard[i].length; j++) {
                count = 0;
                if(gameBoard[i][j].getType() == playerColor){
                    for (int c = 0; c < 4; c++) {
                        if(gameBoard[i+c][j-c].getType() == playerColor){
                            count++;
                            if (count == 4){
                                isWin = true;
                                break;
                            }
                        } else {
                            count = 0;
                        }
                    }
                }
            }
        }
        return isWin;
    }
}
