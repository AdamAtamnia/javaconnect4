public class Tile {
    private String type;
    
    //Sets type to "blank" by default.
    public Tile(){
        this.type = "blank";
    }

    //Sets type of a Tile object
    public void setType(String type){
        if(type.equals("red") || type.equals("yellow") || type.equals("blank")){
            this.type = type;
        } else {
            throw new IllegalArgumentException("The type of a Tile can only be red, yellow or blank. Check spelling.");
        }

    }

    //Gets type of a Tile object
    public String getType(){
        return this.type;
    }

    //Prints a Tile object
    public String toString(){
        String token = "Null";
        if(this.type.equals("red")){
            token = "R";
        } else if(this.type.equals("yellow")){
            token = "Y";
        } else if(this.type.equals("blank")){
            token = " ";
        } else {
            token = "error";
        }
        return token;
    }
}
