import java.util.Scanner;
import java.util.InputMismatchException;
public class Connect4{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Board game = new Board();
        final String PLAYER1COLOR = "red";
        final String PLAYER2COLOR = "yellow";
        String choice = mainMenu(scan);
        while(choice.equals("p")){
            System.out.println(game);
            while(true){
                playerTurn(scan, game, 1, PLAYER1COLOR);
                System.out.println(game);
                
                if(game.checkWin(PLAYER1COLOR) == true){
                    System.out.println("P1 ("+PLAYER1COLOR+") Wins!!!\n ");
                    break;
                } else if(game.isTie() == true){
                    System.out.println("It's a Tie!!!\n ");
                    break;
                }

                playerTurn(scan, game, 2, PLAYER2COLOR);
                System.out.println(game);
                
                if(game.checkWin(PLAYER2COLOR) == true){
                    System.out.println("P2 ("+PLAYER2COLOR+") Wins!!!\n ");
                    break;
                } else if(game.isTie() == true){
                    System.out.println("It's a Tie!!!\n ");
                    break;
                }
            } 
            game.reset();
            choice = mainMenu(scan);  
        }
    }

    //Displays the main menu and only allows the user to input (p) to play or (q) to quit.
    public static String mainMenu(Scanner scan){
        System.out.println("             CONNECT 4");
        System.out.println("----------------------------------");
        System.out.println("             Main Menu\n             =========");
        System.out.println("----------------------------------");
        System.out.println("Instructions:");
        System.out.println("Each player takes turns letting");
        System.out.println("their tokens slide to the bottom of");
        System.out.println("the board by choosing a column.");
        System.out.println("To do so enter the number of the");
        System.out.println("column displayed at the top of the");
        System.out.println("board. The first to connect 4");
        System.out.println("tokens horizontally, vertically or");
        System.out.println("diagonally wins the game!");
        System.out.println("----------------------------------");
        System.out.print("Enter (p) to play, (q) to quit: ");
        
        String choice = scan.next().toLowerCase();
        while(!choice.equals("p") && !choice.equals("q")){
            System.out.println("Your input can only be (p) to play or (q) to quit.");
            choice = scan.next().toLowerCase();
        }
        return choice;
    }

    //Plays a turn for the given player (only 2 players).
    public static void playerTurn(Scanner scan, Board game, int playerNumber, String playerColor){
        if (playerNumber < 1 || playerNumber > 2){
            throw new IllegalArgumentException("Player number can only be 1 or 2");
        } 
        System.out.println("Player "+playerNumber+"'s turn ("+playerColor+")\nChoose your column (1-7): ");

        validateInputAndPlaceToken(scan, game, playerColor);
        
    }
    //Asks the user to retry if input is a String, column chosen is out of range, or if chosen column is full.
    public static void validateInputAndPlaceToken(Scanner scan, Board game, String playerColor){
        int pos = 0;
        pos = checkForTextInput(scan);

        while(game.inputValidation(pos) == "OutOfBounds" || game.inputValidation(pos) == "ColumnFull"){
            if (game.inputValidation(pos) == "OutOfBounds"){
                System.out.println("Input has to be an integer between 1 and 7 inclusively. Please retry:");
            }
            else {
                System.out.println("You cannot place your token in a column that is full. Please retry:");
            }
            pos = checkForTextInput(scan);
        }
        
        if (game.inputValidation(pos) == "Success"){
            game.placeToken(pos, playerColor);
        }
    }
    //Asks the user to retry if input is a String.
    public static int checkForTextInput(Scanner scan){
        int pos = 0;
        boolean isSuccess = false;
        while(!isSuccess){
            try {
                pos = scan.nextInt();
                isSuccess = true;
            }
            catch(InputMismatchException e) {
                System.out.println("Input cannot be text. It has to be an integer between 1 and 7 inclusively. Please retry:");
                scan.next();
            } 
        }
        return pos;
    }
}